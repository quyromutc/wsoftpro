<?php 
   
   class App
   {
   	 //http://localhost/PHP_MVC/home/sayhi/1/2/3
   	 protected $controller="Home";
   	 protected $action="SayHi";
   	 protected $params=[];
	   	 function __construct()
		   	 {   
		   	 		//Array ( [0] => home [1] => 32 [2] => 4 [3] => 1 )
		   	 	 $arr=$this->UrlProcess(); 			
		             // print_r($arr);

		             //XỬ LÝ CONTROLER
	             if (file_exists("../PHP_MVC/mvc/controllers/".$arr[0].".php")) {
	             	 $this->controller=$arr[0];
	             	 unset($arr[0]);
	             require_once "../PHP_MVC/mvc/controllers/".$this->controller.".php";

	            $this->controller = new $this->controller; 
	           	  //xử lý action (function trong home)

	             if (isset($arr[1])) 
	             {
	             	if (method_exists($this->controller, $arr[1])) 
	             	{
	             	 $this->action= $arr[1];
	             	}
	             	unset($arr[1]);
	             }
		             // echo $this->action;
		             //XỬ LÝ PARAMS
	             $this->params=$arr?array_values($arr):[];

	             call_user_func_array([$this->controller,$this->action], $this->params);
	             //tạo ra biến có kiểu controler và nó chạy hàm tên là sayhi với tham số truyền vào như trên, và giá trị là mặc định truyền vào trên khai báo
                 //call_user_func_array([tên lớp,hàm chạy],param)
		             // echo "$this->controllers"."<br>";
		             // echo "$this->action"."<br>";
		             // print_r($this->params);

	         	}


		   	 }
 
	   	 function UrlProcess()
		   {
		   	if(isset($_GET["url"]))
			   	{
			   		return explode("/", filter_var(trim($_GET["url"],"/")));
			   	}
		   }
 	}
   
 ?>



<!--   filter_var đảm bảo dữ liệu sạch,loại bỏ từ lạ 
 trim($str) loại bỏ khoảng trắng trong chuỗi hoặc 
 explode cắt chuỗi thành mảng ngăn cách bởi dấu 

 Hàm trim() sẽ loại bỏ khoẳng trắng( hoặc bất kì kí tự nào được cung cấp) dư thừa ở đầu và cuối chuỗi.

Cú pháp: trim( $str, $char);
 $str là chuỗi cần loại bỏ các kí tự.
 $char là tham số không bắt buộc quy định các kí tự sẽ bị loại bỏ ở đầu và cuối chuỗi. Nếu không truyền, hàm trim() sẽ loại bỏ khoảng trắng.


  Hàm explode() trong php có nhiệm vụ chuyển một chuỗi thành một mảng và mỗi phần tử được cắt bởi một chuỗi con nào đó. Ví dụ ta có chuỗi "học lập trình online freetuts.net" thì nếu ta dùng hàm explode() để chuyển thành mảng và chuỗi con phân cách là khoảng trắng thì ta sẽ có một mảng gồm 5 phần tử gồm ['học', 'lập', 'trình', 'online', 'freetuts.net] 

  Cú pháp: array explode ( string $delimiter , string $string [, int $limit ] )
 Trong đó: 

 $delimiter là chuỗi con dùng để chia các phần tử
 $string là chuỗi cần chuyển đổi
 $limit không cần thiết sử dụng, nó chỉ cần thiết khi bạn muốn mảng trả về có bao nhiêu phần tử thì truyền limit vào con số bấy nhiêu. Nếu bạn truyền vào số bé hơn tổng số phần tử có thể chuyển đổi thì phần tử cuối cùng sẽ không được cắt nhỏ. 

method_exits($object,$method_name) ->Kiểm tra  phương thức  lớp có tồn tại trong đối tượng đã cho.
Một thể hiện đối tượng hoặc tên lớp
Tên phương thức -->